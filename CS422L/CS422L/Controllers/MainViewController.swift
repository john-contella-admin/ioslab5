//
//  ViewController.swift
//  CS422L
//
//  Created by Jonathan Sligh on 1/29/21.
//
import CoreData
import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource  {
    var flashcardsets: [FlashcardSetEntity] = []
    
    @IBOutlet var collectionView: UICollectionView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
        makeThingsLookPretty()
        loadData()
    }

    
    @IBAction func addNewSet(_ sender: Any) {
        var newSets: [FlashcardSetEntity] = []
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let newSet = NSEntityDescription.insertNewObject(forEntityName: "FlashcardSetEntity", into: context) as! FlashcardSetEntity
        newSet.title = "Title \(flashcardsets.count + 1)"
        newSets.append(newSet)
        do {
            try context.save()
        }
        catch{
            print(error)
        }
        self.updateData(sets: newSets)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return flashcardsets.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SetCell", for: indexPath) as! FlashcardSetCollectionCell
        //setup method just makes it look nice
        cell.setup()
        cell.textLabel.text = flashcardsets[indexPath.row].title
        return cell
    }
    

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //go to new view
        performSegue(withIdentifier: "GoToDetail", sender: self)
    }
    
    //another function to make things look nice
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let noOfCellsInRow = 2   //number of column you want
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(noOfCellsInRow - 1))

        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(noOfCellsInRow))
        return CGSize(width: size, height: size)
    }
    
    //just a function to make things look nice
    func makeThingsLookPretty()
    {
        let margin: CGFloat = 10
        guard let collectionView = collectionView, let flowLayout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        flowLayout.minimumInteritemSpacing = margin
        flowLayout.minimumLineSpacing = margin
        flowLayout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
    }
    
    func loadData() {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "FlashcardSetEntity")
            do {
                let sets = try context.fetch(fetchRequest) as! [FlashcardSetEntity]
                updateData(sets: sets)
            } catch {
                print(error)
            }
        }
    func updateData(sets: [FlashcardSetEntity]) {
        self.flashcardsets = sets
        self.collectionView.reloadData()
    }

}

